import scrapy
from ..items import Products
from scrapy_splash import SplashRequest
from time import sleep

class ProductScraper(scrapy.Spider):
    name = 'product_scrapper'
    page_number = 2
    # script = """
    # function main(splash, args)
    #     assert(splash:go(args.url))
    #     assert(splash:wait(5))
    #     splash:set_viewport_full()
    #     return {
    #         html = splash:html(),
    #         png = splash:png(),
    #         har = splash:har(),
    # }
    # end
    # """

    items = Products()

    def start_requests(self):
        url = 'https://www.tokopedia.com/search?st=product&q=iphone'
        script = """
        function main(splash, args)
            assert(splash:go(args.url))
            assert(splash:wait(5))
            splash:set_viewport_full()
            return {
                html = splash:html(),
                png = splash:png(),
                har = splash:har(),
        }
        end
        """
        yield SplashRequest(
            url = url, callback=self.parse, endpoint='execute', priority=1, args={'lua_source' : script}
        )

    def parse(self, response):
        for tr in response.xpath("//div[@class='vlEGRFVq']"):
            p_name = tr.xpath('.//h3/text()').extract_first()
            p_price = tr.xpath('.//div/span/span/text()').extract_first()
            s_city = tr.xpath(".//span[@class='_3ME2eGXf']/text()").extract_first()
            s_name = tr.xpath(".//span[@class='_2rQtYSxg']/text()").extract_first()

            print("Product Name: "+p_name)
            print("Product Price: "+p_price)
            print("Store City: "+s_city)
            print("s_name: "+s_name)

            self.items['product_name'] = str(p_name)
            self.items['product_price'] = str(p_price)
            self.items['store_city'] = str(s_city)
            self.items['store_name'] = str(s_name)

            yield self.items
        
        for i in range(2, 3):
            print(i)
            next_url = 'https://www.tokopedia.com/search?page='+str(i)+'&st=product&q=iphone'
            print(next_url)
            script = """
            function main(splash, args)
                assert(splash:go(args.url))
                assert(splash:wait(5))
                splash:set_viewport_full()
                return {
                    html = splash:html(),
                    png = splash:png(),
                    har = splash:har(),
            }
            end
            """
            yield SplashRequest(
                url = next_url, callback=self.parse_next, endpoint='execute', priority=i, args={'lua_source' : script}
            )
            ## loop for pagination
    def parse_next(self, response):
        for tr in response.xpath("//div[@class='vlEGRFVq']"):
            self.items['product_name'] = tr.xpath('.//h3/text()').extract_first(),
            self.items['product_price'] = tr.xpath('.//div/span/span/text()').extract_first()
            self.items['store_city'] = tr.xpath(".//span[@class='_3ME2eGXf']/text()").extract_first()
            self.items['store_name'] = tr.xpath(".//span[@class='_2rQtYSxg']/text()").extract_first()

            yield self.items  