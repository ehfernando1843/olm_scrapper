# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import mysql.connector
import sqlite3

class ProjectPipeline(object):
    def __init__(self):
        self.create_connection()
        self.prepare_table()

    def process_item(self, item, spider):
        print("product_name: "+item['product_name'])
        print("product_price: "+item['product_price'])
        print("store_city: "+item['store_city'])
        print("store_name: "+item['store_name'])
        self.store_db(item)
        return item

    def create_connection(self):
        self.conn = mysql.connector.connect(
            host = '',
            user = '',
            passwd = '',
            database = ''
        )
        # self.conn = sqlite3.connect("products")
        self.curr = self.conn.cursor()

    def prepare_table(self):
        self.curr.execute("""
            create table if not exists products(
                p_name text,
                p_price text,
                s_location,
                s_name
            )
        """)

    def store_db(self, item):
        self.curr.execute("""insert into products values (%s,%s,%s,%s)""", (
            item['product_name'],
            item['product_price'],
            item['store_city'],
            item['store_name']
        ))
        self.conn.commit()



